'use strict';

function calcAge(birthYear) {
    const age = 2022 - birthYear;

    function printAge() {
        let output = `${firstName}, You are ${age}, born in ${birthYear}`;
        console.log(output);

        if (birthYear >= 1981 && birthYear <= 1996) {
            // Creating a NEW variable with same name as outer scope's variable 

            const firstName = 'Kibet';

            //Reassigning outer scope's variable 
            output = 'NEW OTPUT';

            const str = `${firstName}, you are a millenial`;
            console.log(str);


            function add(a, b) {
                return a + b;
            }
            console.log(add(2, 3));
        }
    }
    printAge();
    return age
}
const firstName = 'Brian';
calcAge(1996);

// Hoisting with variables

console.log(me);
// console.log(job);
// console.log(year);

var me = 'Brian';
let job = 'Teacher';
const year = 1996;

// Funcrions

console.log(addDecaration(2, 3));
// console.log(addExpression(2, 3));
// console.log(addArrow());

function addDecaration(a, b) { // Function Declaration
    return a + b;
}

const addExpression = function(a, b) { // Function Expression
    return a + b;
}

const addArrow = (a, b) => a + b; // Arrow Function

//This keyword
console.log(this);

const calcAgee = function(birthYear) {
    console.log(2037 - birthYear);
    console.log(this);
}
calcAgee(1996);

const calcAgeeArrow = birthYear => {
    console.log(2037 - birthYear);
    console.log(this);
}
calcAgeeArrow(1999);

// const tanui = {
//     year: 1996,
//     calAge: function() {
//         console.log(this);
//         console.log(2022 - this.year);
//     }
// }
//tanui.calAge();

const kiptanui = {
    year: 2018,
}

//Method Borrowing
//kiptanui.calAge = tanui.calAge;
//kiptanui.calAge();

const tanui = {
    firstName: 'Brian',
    year: 1996,
    calAge: function() {
        console.log(this);
        console.log(2022 - this.year);

        //Solution 1

        // const self = this; //Self or that
        // const isMillenial = function() {
        //     console.log(self.year >= 1981 && self.year <= 1996);
        //     // console.log(this.year >= 1981 && this.year <= 1996);
        // }

        //Solution 2

        const isMillenial = () => {
            console.log(this.year >= 1981 && this.year <= 1996);
        }

        isMillenial();
    },
    greet: () => console.log(`Hey ${this.firstName}`),
}
tanui.greet();
tanui.calAge();


//arguments keyword
const addExp = function(a, b) { // Function Expression
    console.log(arguments);
    return a + b;
}

addExp(2, 5);

const addArroww = (a, b) => {
    //  console.log(arguments);
    return a + b;
}; // Arrow Function
addArroww(2, 3, 5);

const mee = {
    name: 'Brian',
    age: 26,
}
const friend = mee;
friend.age = 30;

console.log('Kim', friend);
console.log('Me', mee);

//Primitive Types
let lastName = 'Brian';
let oldLastName = lastName;
lastName = 'David';
console.log(lastName, oldLastName);


//Reference types
const jessica = {
    firstName: 'Jess',
    lasName: 'Williams',
    age: 27,
};
const marriedJessica = jessica;
marriedJessica.lasName = 'Davis';
console.log('Before Marriage:', jessica);
console.log('After Marriage:', marriedJessica);

//Copying objects

const jessica2 = {
    firstName: 'Jess',
    lastName: 'Williams',
    age: 27,
    family: ['sabina', 'lilian'],
};

const jessicaCopy = Object.assign({}, jessica2);
jessicaCopy.lastName = 'Brian';

jessicaCopy.family.push('James');
jessicaCopy.family.push('Lucy');

console.log('Before Marriage:', jessica2);
console.log('After Marriage:', jessicaCopy);